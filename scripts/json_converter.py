from bs4 import BeautifulSoup


class JsonConverter(object):
    def get_json_format(self, content):
        raise "To implement!"


class CSVToJsonStrategy(JsonConverter):
    def get_json_format(self, content):
        results = []
        for line in content.split():
            results.append({'url': line})
        return results


class XmlToJsonStrategy(JsonConverter):
    def get_json_format(self, content):
        results = []
        soup = BeautifulSoup(content)
        for url in soup.findAll('url'):
            results.append({'url': url.find('loc').string})
        return results
