import csv


class CSVSaver(object):
    @staticmethod
    def save(file_name, links):
        with open('sitemaps/' + file_name + '.csv', 'wb') as sitemap_file:
            writer = csv.writer(sitemap_file,
                                quotechar='|',
                                quoting=csv.QUOTE_MINIMAL)
            for link in links:
                writer.writerow([link])


class XMLSaver(object):

    @staticmethod
    def save(file_name, response):
        with open('sitemaps/' + file_name + '.xml', 'wb') as sitemap_file:
            sitemap_file.write(response.text)
