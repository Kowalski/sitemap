from robot import RobotParser
import requests
from bs4 import BeautifulSoup

class SiteParser(object):

    ROBOTS_URL = '/robots.txt'

    def __init__(self, url, robot_enable=False):
        self.url = url
        self.robot_enable = robot_enable
        self.inner_links = []
        self.outer_links = []

    def parse(self):
        self.add_all_links()
        if self.robot_enable:
            self.exclude_disallow()

    def add_all_links(self):
        response = requests.get(self.url)
        soup = BeautifulSoup(response.text, "lxml")

        for link in soup.findAll('a'):
            try:
                if self.url in link['href']:
                    self.inner_links.append(link['href'])
                elif 'http' in link['href'] or 'https' in link['href']:
                    self.outer_links.append(link)
                else:
                    self.inner_links.append(self.url + link['href'])
            except KeyError:
                print "Key error occured"

    def get_inner_links(self):
        return self.inner_links

    def get_outer_links(self):
        return self.outer_links

    def exclude_disallow(self):
        robots = RobotParser(self.url)
        robots.run()
        disallow_urls = robots.get_disallow_urls()
        for disallow in disallow_urls:
            self.inner_links = [correct_url for correct_url in self.inner_links if disallow not in self.inner_links]
