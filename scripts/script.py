import requests
import re
from csv_generator import CSVSaver, XMLSaver
from site_parser import SiteParser


def generate_or_download_sitemap(url):
    response = requests.get(url + '/sitemap.xml')
    file_name = prepare_file_name(url)
    if response:
        XMLSaver().save(file_name, response)
    else:
        site = SiteParser(url, True)
        site.parse()
        inner_links = site.get_inner_links()
        CSVSaver().save(file_name, inner_links)


def prepare_file_name(url):
    regexp = r'.+\/\/'
    results = re.search(regexp, url)
    if results:
        return url.replace(results.group(0), '')


links = ['http://cnn.com', 'https://www.nazwa.pl']

for link in links:
    generate_or_download_sitemap(link)
