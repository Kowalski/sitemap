import os


class FileFinder(object):
    SITEMAPS_PATH = "/sitemaps"
    EXTENSIONS = ['.csv', '.xml']

    def get_file_content(self, url):
        for extention in self.EXTENSIONS:
            res = self.find(url + extention)
            if res:
                return res, extention
        return None, None

    def find(self, file_name):
        absolute_path = os.path.dirname(os.path.abspath(__file__))
        dir_list = os.listdir(absolute_path + self.SITEMAPS_PATH)
        if file_name in dir_list:
            with open(absolute_path + self.SITEMAPS_PATH + '/' + file_name, 'r') as content_file:
                return content_file.read()
        return None
