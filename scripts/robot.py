import requests
import re

class RobotParser(object):

    ROBOTS_URL = '/robots.txt'
    REGEXP_DISALLOW = r'\/.*'

    def __init__(self, url):
        self.robots_url = url + self.ROBOTS_URL
        self.disallow_urls = []

    def run(self):
        response = requests.get(self.robots_url)
        if response:
            self.parse_robots_rules(response.text)

    def parse_robots_rules(self, content):
        self.disallow_urls = re.findall(self.REGEXP_DISALLOW, content)

    def get_disallow_urls(self):
        return self.disallow_urls