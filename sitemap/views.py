from rest_framework.response import Response
from rest_framework.views import APIView
from scripts.file_finder import FileFinder
from scripts.json_converter import XmlToJsonStrategy, CSVToJsonStrategy


class SiteMapView(APIView):

    def get(self, request):
        query_params = request.query_params
        url = query_params.get('url', None)

        file_content, extention = FileFinder().get_file_content(url)
        if not file_content:
            return Response("Data doesn't exist")
        if extention == '.csv':
            return Response(CSVToJsonStrategy().get_json_format(file_content))
        else:
            return Response(XmlToJsonStrategy().get_json_format(file_content))


